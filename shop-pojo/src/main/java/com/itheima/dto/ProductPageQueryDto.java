package com.itheima.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductPageQueryDto {

    private Integer page; //页码
    private Integer pageSize; //每页展示记录数
    private String name; //商品名称
    private Integer categoryId; //商品分类
    private Integer brandId; //商品品牌
    private Integer publishStatus; //上架状态 , 0: 下架 , 1: 上架
    private Integer verifyStatus; //审核状态, 0: 未审核 , 1: 审核通过

}
