package com.itheima.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeePageQueryDto {

    private Integer page; //页码
    private Integer pageSize; //每页展示记录数
    private String realName; //姓名
    private String email; //邮箱
    private String username; //用户名

}
