package com.itheima.enumeration;

/**
 * 数据库操作类型
 */
public enum OperationType {

    /**
     * 插入操作
     */
    INSERT,

    /**
     * 更新操作
     */
    UPDATE,

    /**
     * 删除操作
     */
    DELETE,

    /**
     * 查询操作
     */
    SELECT
}
