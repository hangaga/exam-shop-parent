package com.itheima.utils;

import com.itheima.constant.TokenConstant;
import io.jsonwebtoken.Claims;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

public class CurrentLoginHelper {

    /**
     * 获取当前登录用户ID
     */
    public static Integer getCurrentId(){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token = request.getHeader(TokenConstant.TOKEN_HEADER);
        if(StringUtils.isNotEmpty(token)){
            Claims claims = JwtUtil.parseJWT(token);
            if(claims != null){
                return Integer.valueOf(String.valueOf(claims.get("id")));
            }
        }
        return null;
    }

}
