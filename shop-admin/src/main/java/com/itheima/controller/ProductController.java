package com.itheima.controller;

import com.itheima.dto.ProductPageQueryDto;
import com.itheima.pojo.Product;
import com.itheima.result.PageResult;
import com.itheima.result.Result;
import com.itheima.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 商品管理
 */
@Slf4j
@Api(tags = "商品管理")
@RestController
@RequestMapping("/shop/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    /**
     * 分页查询
     */
    @ApiOperation("分页查询")
    @GetMapping
    public Result<PageResult> page(ProductPageQueryDto pageQueryDto) {
        PageResult pageResult = productService.page(pageQueryDto);
        return Result.success(pageResult);
    }

    /**
     * 保存商品
     */
    @ApiOperation("保存商品")
    @PostMapping
    public Result save(@RequestBody Product product) {
        productService.save(product);
        return Result.success();
    }

    /**
     * 更新商品
     */
    @ApiOperation("更新商品")
    @PutMapping
    public Result updateById(@RequestBody Product product) {
        productService.updateById(product);
        return Result.success();
    }

    /**
     * 删除商品
     */
    @ApiOperation("删除商品")
    @DeleteMapping("/{id}")
    public Result removeById(@PathVariable Integer id) {
        productService.delete(id);
        return Result.success();
    }

    /**
     * 根据ID查询商品
     */
    @ApiOperation("根据ID查询商品")
    @GetMapping("/{id}")
    public Result getById(@PathVariable Integer id) {
        Product product = productService.getById(id);
        return Result.success(product);
    }

    /**
     * 上下架
     */
    @ApiOperation("上下架")
    @PutMapping("/publish/{id}/{publishStatus}")
    public Result upOrDown(@PathVariable Integer id, @PathVariable Integer publishStatus) {
        productService.upOrDown(id, publishStatus);
        return Result.success();
    }


    /**
     * 审核
     */
    @ApiOperation("商品审核")
    @PutMapping("/verify/{id}/{status}")
    public Result verify(@PathVariable Integer id, @PathVariable Integer status) {
        productService.verify(id, status);
        return Result.success();
    }

}
