package com.itheima.controller;

import com.itheima.dto.EmployeePageQueryDto;
import com.itheima.pojo.Employee;
import com.itheima.result.PageResult;
import com.itheima.result.Result;
import com.itheima.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 员工管理
 */
@Slf4j
@Api(tags = "员工管理")
@RestController
@RequestMapping("/shop/employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    /**
     * 新增员工
     */
    @ApiOperation("新增员工")
    @PostMapping
    public Result save(@RequestBody Employee employee){
        log.info("新增员工, {}", employee);
        employeeService.save(employee);
        return Result.success();
    }


    /**
     * 分页查询
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public Result<PageResult> page(EmployeePageQueryDto pageQueryDTO){
        log.info("条件分页查询, {}" , pageQueryDTO);
        PageResult pageResult = employeeService.page(pageQueryDTO);
        return Result.success(pageResult);
    }


    /**
     * 启用/禁用员工
     */
    @ApiOperation("启动/禁用员工")
    @PutMapping("/status/{status}/{id}")
    public Result enableOrDisable(@PathVariable Integer status , @PathVariable Integer id){
        log.info("启用/禁用员工 , {}, {}", status, id);
        employeeService.enableOrDisable(status, id);
        return Result.success();
    }


    /**
     * 根据ID查询员工信息
     */
    @GetMapping("/{id}")
    @ApiOperation("根据ID查询员工信息")
    public Result<Employee> getById(@PathVariable Integer id){
        log.info("根据ID查询员工信息, id:{}", id);
        Employee employee = employeeService.getById(id);
        return Result.success(employee);
    }

    /**
     * 编辑员工信息
     */
    @PutMapping
    @ApiOperation("编辑员工信息")
    public Result update(@RequestBody Employee employee){
        log.info("编辑员工: {}", employee);
        employeeService.update(employee);
        return Result.success();
    }

}
